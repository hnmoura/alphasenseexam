import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Hivison N Moura (hivison.moura@wavy.global)
 * Creation Date 25/06/19
 */
public abstract class Digest {


    private Map<byte[], byte[]> cache = new HashMap<byte[], byte[]>();

    public byte[] digest(byte[] input) {
        byte[] result = cache.get(input);
        if(result == null){
            synchronized (cache) {
                result = cache.get(input);
                if (result == null){
                    try {
                        result = doDigest(input);
                        cache.put(input, result);
                    } catch (RuntimeException ex) {
                        LoggerFactory.getLogger("Digest").error(
                                "Unable to make digest"
                        );
                        throw ex;
                    }
                }
            }
        }
        return result;
    }

    protected abstract byte[] doDigest(byte[] input);
}

/*
 * Issues:
 *
 * On line 13 you can use generic instead of re-declare the HashMap types
 * On line 16 check if the result is null instead of test if the array is empty
 * On line 18 the modifier synchronized is used on a non-final param
 */
