import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;


public class Task1 {

    public int getUniqueCharacterSubstring(String input) {

        if(StringUtils.isNotBlank(input)){
            Map<Character, Integer> visited = new HashMap<>();
            String output = StringUtils.EMPTY;
            for (int start = 0, end = 0; end < input.length(); end++) {
                char currChar = input.charAt(end);
                if (visited.containsKey(currChar)) {
                    start = Math.max(visited.get(currChar)+1, start);
                }
                if (output.length() < end - start + 1) {
                    output = input.substring(start, end + 1);
                }
                visited.put(currChar, end);
            }
            return output.length();
        }
        return 0;
    }
}