
public class Task2 {

    Boolean maxSubArraySum(int arr[], int sum) {

        int n = arr.length;
        int currSum = arr[0];
        int start = 0;
        int i;

        for (i = 1; i <= n; i++) {
            while (currSum > sum && start < i - 1) {
                currSum = currSum - arr[start];
                start++;
            }
            if (currSum == sum)
                return true;

            if (i < n)
                currSum = currSum + arr[i];

        }

        return false;
    }
}
