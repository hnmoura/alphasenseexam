import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class Task1Test {
    
    private Task1 task1 = new Task1();

    @Test
    void givenString_whenGetUniqueCharacterSubstringCalled_thenResultFoundAsExpected() {
        assertEquals(1, task1.getUniqueCharacterSubstring("A"));
        assertEquals(6, task1.getUniqueCharacterSubstring("AABCDEF"));
        assertEquals(6, task1.getUniqueCharacterSubstring("ABCDEFF"));
        assertEquals(7, task1.getUniqueCharacterSubstring("CODINGISAWESOME"));
        assertEquals(3, task1.getUniqueCharacterSubstring("ABCABCBB"));
        assertEquals(1, task1.getUniqueCharacterSubstring("BBBBB"));
        assertEquals(3, task1.getUniqueCharacterSubstring("PWWKEW"));
        assertEquals(0, task1.getUniqueCharacterSubstring(null));
        assertEquals(0, task1.getUniqueCharacterSubstring(StringUtils.EMPTY));
    }

}