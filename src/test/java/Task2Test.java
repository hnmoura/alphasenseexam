import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class Task2Test {

    private Task2 task2 = new Task2();

    @Test
    void givenPositiveIntArray_whenMaxSubArraySumCalled_thenResultFoundAsExpected(){
        int[] array1 = {1,2,3,4};
        int[] array2 = {1,1,1};
        int[] array3 = {10, 20, 200};
        assertEquals(true,task2.maxSubArraySum(array1, 7));
        assertEquals(true, task2.maxSubArraySum(array2, 2));
        assertEquals(false, task2.maxSubArraySum(array3, 999));

    }

}